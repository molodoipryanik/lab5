"""@package docstring
Практическая работа по ВВПД №5 Луцив Михаила"""
import math


def enter(x):
    """Функция нужна для проверки коррекности входных данных радиусов окружностей
    и сортировки радиусов в порядке увеличения их размеров.
    Args:
    x: список радиусов окружностей с общим центром.
    Returns:
    отсортированный список уникальных радиусов.
    Raises:
    ValueError, TypeError, ZeroDivisionError
    Examples:
    >>> enter(["1"]) \n
    [1]
    >>> enter(["3", "2","1"]) \n
    [1, 2, 3]
    >>> enter(" ") \n
    Traceback (most recent call last): \n
        ... \n
    ValueError: invalid literal for int() with base 10: ' ' \n
    >>> enter([0]) \n
    Traceback (most recent call last): \n
        ... \n
    TypeError \n
    >>> enter(["anime", 1, 2]) \n
    Traceback (most recent call last): \n
        ... \n
    ValueError: invalid literal for int() with base 10: 'anime' \n
    >>> enter(['0']) \n
    Traceback (most recent call last): \n
        ... \n
    ZeroDivisionError """
    if not all(i for i in x if isinstance(i, int)):
        raise TypeError()
    x = sorted(map(int, x))
    if not any(x):
        raise ZeroDivisionError()
    if sum(x) != sum(abs(i) for i in x) or not all(x):
        raise ValueError()
    return x


def print_s_and_p(rs):
    """Функция нужна для подсчета суммарной площади всех красных областей на
    плоскости, суммарной площади всех синих областей на плоскости, вероятности
    попадания в красную область и вероятности попадания в синюю область.
    Args:
    rs: список радиусов, прошедший проверку, окружностей с общим центром.
    Returns:
    кортеж из площади красных колец, площади синих колец,
    вероятности красных колец, вероятности синих колец. \n
    Examples:
    >>> print_s_and_p([2, 4])  \n
    (12.5664, 37.6991, 0.25, 0.75) \n
    >>> print_s_and_p([1, 2, 3, 4, 5]) \n
    (47.1239, 31.4159, 0.6, 0.4) \n
    >>> print_s_and_p([2, 4, 6]) \n
    (75.3982, 37.6991, 0.67, 0.33)"""
    s_red = round(sum([math.pi * i ** 2 for i in rs[::2]]) - sum([math.pi * i ** 2 for i in rs[1:-1:2]]), 4)
    s_blue = round(sum([math.pi * k ** 2 for k in rs[1::2]]) - sum([math.pi * k ** 2 for k in rs[:-1:2]]), 4)
    return s_red, s_blue, round(s_red / (s_red + s_blue), 2), round(s_blue / (s_red + s_blue), 2)


def main():
    """Функция соединяет в себе две функции, то есть одновременно проверяет входные данные и выводит результат.
    Returns:
    площадь красных колец, площадь синих колец,
    вероятность красных колец, вероятность синих колец.
    Examples:
    >>> main() \n
    >? 1 2 3 \n
    18.8496 9.4248 0.67 0.33 \n
    >>> main() \n
    >? 2 3 4 \n
    34.5575 15.708 0.69 0.31 \n
    >>> main() \n
    >? 5 \n
    78.5398 0 1.0 0.0"""
    print(*print_s_and_p(enter(set(input().split()))))


if __name__ == "__main__":
    main()
