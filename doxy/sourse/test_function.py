from IntroductionLab3 import enter
from IntroductionLab3 import print_s_and_p
import pytest


def test_sort_enter():
    """ Три радиуса в разном порядке """
    assert enter(['4', '2', '5']) == [2, 4, 5]


def test_3r_enter():
    """ Три радиуса в нужном порядке """
    assert enter(['2', '4', '5']) == [2, 4, 5]


def test_2r_enter():
    """ Два радиуса в нужном порядке """
    assert enter(['2', '4']) == [2, 4]


def test_1r_enter():
    """ Один радиус в нужном порядке """
    assert enter(['2']) == [2]


def test_empty_enter():
    """ Пустая строка """
    with pytest.raises(ValueError):
        enter(" ")


def test_words_enter():
    """ Некорректные данные """
    with pytest.raises(ValueError):
        enter(["anime", 1, 2])


def test_zero_enter():
    """ Нулевой радиус """
    with pytest.raises(ZeroDivisionError):
        enter(["0"])


def test_negative_r_enter():
    """ Отрицательный радиус """
    with pytest.raises(ValueError):
        enter(["-2", "4", "5"])


def test_1r_print_s_and_p():
    """ Подсчет площадей колец из трех радиусов и их вероятностей """
    assert print_s_and_p([2, 4, 5]) == (40.8407, 37.6991, 0.52, 0.48)


def test_2r_print_s_and_p():
    """ Подсчет площадей колец из двух радиусов и их вероятностей """
    assert print_s_and_p([2, 4]) == (12.5664, 37.6991, 0.25, 0.75)


def test_1r_print_p_and_s():
    """ Подсчет площадей колец из одного радиуса и их вероятностей """
    assert print_s_and_p([2]) == (12.5664, 0, 1.0, 0.0)


def test_4r_print_s_and_p():
    """ Подсчет площадей колец из четырех радиусов и их вероятностей """
    assert print_s_and_p([1, 2, 3, 4]) == (18.8496, 31.4159, 0.38, 0.62)


def test_5r_print_s_and_p():
    """ Подсчет площадей колец из пяти радиусов и их вероятностей """
    assert print_s_and_p([1, 2, 3, 4, 5]) == (47.1239, 31.4159, 0.6, 0.4)
