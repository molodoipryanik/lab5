# Практическая работа №5 

Для начала нужно было выполнить поставленные задачи:

1. Разделить логически программу на три фунции.
  * функция проверки ввода.
  * функция выполнения прораммы.
  * функция вывода результата программы на экран.
2. Написать документацию к каждой из них.
3. Создать с помощью Doxygen html страничку с документацией.
4. Оформить readme.md, в котором можно будет увидеть этапы проделанной работы.
5. __Посматреть аниме__

Для выполнения практической надо было воспользоваться формулой:

$`S = Pi(R^2 - r^2)`$

Алгоритм решения задачи заключается в высчитывании площадей колец четных и нечетных колец
и последующее их сложение, а также высчитывание площади большей окружности для опредения
вероятности появления четных и нечетных колец путем их деления.


_Функция вывода._
```
main():
    print(*print_s_and_p(enter(set(input().split()))))
```

_Функция проверки ввода._
```
enter(x):
    if not all(i for i in x if isinstance(i, int)):
        raise TypeError()
    x = sorted(map(int, x))
    if not any(x):
        raise ZeroDivisionError()
    if sum(x) != sum(abs(i) for i in x) or not all(x):
        raise ValueError()
    return x
```

_Функция выполнения задания._
```
print_s_and_p(rs):
    s_red = round(sum([math.pi * i ** 2 for i in rs[::2]]) - sum([math.pi * i ** 2 for i in rs[1:-1:2]]), 4)
    s_blue = round(sum([math.pi * k ** 2 for k in rs[1::2]]) - sum([math.pi * k ** 2 for k in rs[:-1:2]]), 4)
    return s_red, s_blue, round(s_red / (s_red + s_blue), 2), round(s_blue / (s_red + s_blue), 2)
```

[Илон Маск](https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%81%D0%BA,_%D0%98%D0%BB%D0%BE%D0%BD)

<a href="https://ibb.co/cyn44Tj"><img src="https://i.ibb.co/BfM00s9/24-1-768x432.jpg" alt="24-1-768x432" border="0"></a>

